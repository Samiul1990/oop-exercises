package ex1_4;

public class Employee {
    int id;
    String firstName;
    String lastName;
    int salary;

    public Employee(int id, String firstName, String lastName, int salary) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public int getId() {
        return this.id;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getName()
    {
        return this.firstName +" " + this.lastName;
    }

    public int getSalary() {
        return this.salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
    public int getAnnualSalary()
    {
        return this.salary * 12;
    }
    public int raiseSalary(int percent){

        int newSalary = (((this.salary * percent ) / 100) + this.salary);
        this.salary = newSalary;
        return newSalary;
    }

    public String toString() {
        return "Employee[" +
                "id= " + this.id +
                ", firstName= " + this.firstName + '\'' +
                ", lastName= " + this.lastName + '\'' +
                ", salary= " + this.salary +
                ']';
    }
}
