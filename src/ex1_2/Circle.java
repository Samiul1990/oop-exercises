package ex1_2;

public class Circle {
    private double radius;

    public Circle() {
        radius = 1.0;
    }
    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return this.radius * this.radius * Math.PI;
    }
    public double getCircumference()
    {
        return 2 * this.radius * Math.PI;
    }
    public String toString() {
        return "Circle[radius=" + this.radius + "]";
    }
}
